let canvas;
let context;
let savedImageData;
let dragging = false;

let strokeColour = "black";
let fillColour = "black";
let line_width = 2;
let canvasWidth = 900;
let canvasHeight = 900;

let brushXPoints = new Array();
let brushYPoints = new Array();
let brushDownPos = new Array();

class MouseDownPos {
    constructor(x,y) {
        this.x = x;
        this.y = y;
    }
}

class Location {
    constructor(x,y) {
        this.x = x;
        this.y = y;
    }
}

let mouseDown = new MouseDownPos(0,0);
let loc = new Location(0,0);

// Call for our function to execute when page is loaded
document.addEventListener('DOMContentLoaded', setupCanvas);

function setupCanvas() {
    canvas = document.getElementById("colour_canvas");
    context = canvas.getContext("2d");
    context.strokeStyle = strokeColour;
    context.lineWidth = line_width;

    AddInitialLine();
    SaveCanvasImage();
    DrawBrush();

    brushDownPos = new Array();

    canvas.addEventListener("mousedown", ReactToMouseDown);
    canvas.addEventListener("mousemove", ReactToMouseMove);
    canvas.addEventListener("mouseup", ReactToMouseUp);
}

function GetMousePosition(x,y) {
    let canvasSizeData = canvas.getBoundingClientRect();
    return { 
        x: (x - canvasSizeData.left) * (canvas.width / canvasSizeData.width),
        y: (y - canvasSizeData.top) * (canvas.height / canvasSizeData.height)
    };
}

function SaveCanvasImage() {
    savedImageData = context.getImageData(0, 0, canvas.width,canvas.height);
}

function RedrawCanvasImage() {
    context.putImageData(savedImageData, 0, 0);
}

function AddInitialLine() {
    for(let i = 1; i < canvas.width; i++) {
        AddBrushPoint(i, Math.sin(i/canvas.height) * canvas.height/2 + canvas.height/2, true);
    }
}

function AddBrushPoint(x, y, mouseDown) {
    brushXPoints.push(x);
    brushYPoints.push(y);
    brushDownPos.push(mouseDown);
}

function ModifyBrushPoint(x, y, mouseDown) {
    if(brushXPoints[x]) {
        brushYPoints[x] = y;
        brushDownPos[x] = mouseDown;
    } else {
        AddBrushPoint(x, y, mouseDown);
    }
}

function DrawBrush() {
    for(let i = 1; i < brushXPoints.length; i++) {
        context.beginPath();
        if(brushDownPos[i]) {
            context.moveTo(brushXPoints[i-1], brushYPoints[i-1]);
        } else {
            context.moveTo(brushXPoints[i]-1, brushYPoints[i]-1);
        }
        context.lineTo(brushXPoints[i], brushYPoints[i]);
        context.stroke();
    }
}

function ReactToMouseDown(e) {
    canvas.style.cursor = "crosshair";
    loc = GetMousePosition(e.clientX, e.clientY);
    mouseDown.x = loc.x;
    mouseDown.y = loc.y;
    dragging = true;
    ModifyBrushPoint(Math.trunc(loc.x), Math.trunc(loc.y), false);
}

function ReactToMouseMove(e) {
    canvas.style.cursor = "crosshair";
    loc = GetMousePosition(e.clientX, e.clientY);

    if(dragging) {
        if(loc.x > 0 && loc.x < canvasWidth && loc.y > 0 && loc.y < canvasHeight) {
            ModifyBrushPoint(Math.trunc(loc.x), Math.trunc(loc.y), true);
        }
        RedrawCanvasImage();
        DrawBrush();
    }
}

function ReactToMouseUp(e) {
    newImage();
    canvas.style.cursor = "default";
    loc = GetMousePosition(e.clientX, e.clientY);
    SaveCanvasImage();
    RedrawCanvasImage();
    DrawBrush();
    dragging = false;
}

function openImage() {

}

function saveImage() {

}

function newImage() {
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.beginPath();
}

setupCanvas();